//
//  FoodTableViewCell.swift
//  InspiRadio
//
//  Created by John Yang on 8/2/16.
//  Copyright © 2016 John Yang. All rights reserved.
//

import Foundation
import UIKit

class FoodTableViewCell: UITableViewCell {
    
    @IBOutlet weak var foodImage: UIImageView!
    @IBOutlet weak var foodName: UILabel!
    @IBOutlet weak var foodAddress: UILabel!
    @IBOutlet weak var foodDistance: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
