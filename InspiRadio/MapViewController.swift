//
//  MapViewController.swift
//  InspiRadio
//
//  Created by John Yang on 7/17/16.
//  Copyright © 2016 John Yang. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import CoreLocation
import UITextField_Shake

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var emotionField: UITextField!
    let emotions: [String] = ["lucky", "happy", "lonely", "peaceful", "okay", "fantastic", "down", "generous", "wonderful", "hungry", "curious", "artistic"]
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var inspireButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emotionField.placeholder = emotions[Int(arc4random_uniform(UInt32(emotions.count)))] + "?"
        emotionField.borderStyle = UITextBorderStyle.RoundedRect
        
        self.hideKeyboardWhenTappedAround()
        self.emotionField.delegate = self //What does this line do?
        
        //Current Location Instantiator
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization() //Requests user for permission to use current location
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        if CLLocationManager.locationServicesEnabled() {
            print("Location Services Enabled")
            locationManager.startUpdatingLocation()
            var region:MKCoordinateRegion = MKCoordinateRegion();
            if let userLocation = locationManager.location?.coordinate {
                region = MKCoordinateRegion(center: userLocation, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
            } else {
                let location = CLLocationCoordinate2D(latitude: 37.773514, longitude: -122.4177)
                region = MKCoordinateRegion(center: location, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
            }
            self.mapView.showsUserLocation = true
            self.mapView.setRegion(region, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func inspireButtonClicked(sender: AnyObject) {
        let test = emotionField.text?.isEmpty
        if test == true {
            emotionField.shake(10, withDelta: 5.0, speed: 0.03)
        } else {
            //return key word and location
        }
    }
    
    //MARK: Dealing with segues:
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        print("entered prepareForSegue")
        if (segue.identifier == "sw_front_news") {
            let viewController = segue.destinationViewController as! NewsViewController
            viewController.emotionLabel.text = self.emotionField.text
            print("entered")
        }
    }
    
    override func didMoveToParentViewController(parent: UIViewController?) {
        if !(parent!.isEqual(self.parentViewController)) {
            print("Back")
        }
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}