//
//  ShoppingViewController.swift
//  InspiRadio
//
//  Created by John Yang on 8/3/16.
//  Copyright © 2016 John Yang. All rights reserved.
//

import Foundation
import UIKit

class ShoppingViewController:UIViewController {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}