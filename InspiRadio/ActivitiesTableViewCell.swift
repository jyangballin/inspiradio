//
//  ActivitiesTableViewCell.swift
//  InspiRadio
//
//  Created by John Yang on 8/3/16.
//  Copyright © 2016 John Yang. All rights reserved.
//

import Foundation
import UIKit

class ActivitiesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var activitiesImage: UIImageView!
    @IBOutlet weak var activitiesName: UILabel!
    @IBOutlet weak var activitiesLocation: UILabel!
    @IBOutlet weak var activitiesDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}