//
//  ActivitiesViewController.swift
//  InspiRadio
//
//  Created by John Yang on 7/20/16.
//  Copyright © 2016 John Yang. All rights reserved.
//

import Foundation

class ActivitiesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var activitiesTableView: UITableView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        self.activitiesTableView.dataSource = self
        self.activitiesTableView.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ActivitiesViewController {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.activitiesTableView.dequeueReusableCellWithIdentifier("ActivitiesCell", forIndexPath: indexPath) as! ActivitiesTableViewCell
        cell.activitiesName.text = "Test"
        
        //set cell elements here
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //cell selected code here
    }
}