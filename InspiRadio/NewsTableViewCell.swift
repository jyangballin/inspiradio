//
//  NewsTableViewCell.swift
//  InspiRadio
//
//  Created by John Yang on 7/22/16.
//  Copyright © 2016 John Yang. All rights reserved.
//

import Foundation
import UIKit

class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var newsArticleName: UILabel!
    @IBOutlet weak var newsArticleThumbnail: UIImageView!
    @IBOutlet weak var newsArticleDescription: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}